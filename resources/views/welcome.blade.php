<!DOCTYPE html>
<html>
<head>
    <title>Tasty Shoppers</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content="">
    <meta name="robots" content="index, follow">

    <!-- Style sheets -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/css/responsiveslides.css" rel="stylesheet">


    <!-- Scripts -->
    <script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.nicescroll.js"></script>
    <script type="text/javascript" src="assets/js/responsiveslides.min.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
    <script>

        $(window).load(function () {
            function masonry() {
                //masonry
                var container = document.querySelector('.masonry');
                var msnry = new Masonry(container, {
                    itemSelector: '.products-box',
                    columnWidth: '.products-box',
                    isFitWidth: true
                });

            };
            masonry();


            $('.quick-product-slide').responsiveSlides({
                manualControls: "#quick-product-thumb-slide"
            });
            $("#quick-product-thumb-slide").owlCarousel({
                navigation: true,
                items: 3
            });
            //slider product
            $("#product-slider").owlCarousel({
                items: 4,
                lazyLoad: true,
                navigation: true
            });
        });

    </script>

</head>
<body>
<!-- Top Header Start Here -->
<div class="top-header">
    <div class="container-fluid custom-container">
        <div class="row">
            <div class="col-md-7">
                <div class="row">
                    <div class="logo-area col-md-2">
                        <img alt="..." src="assets/images/logo.png" class="img-responsive">
                    </div>
                    <div class="search-bar col-md-10">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">All departments <i
                                        class="fa fa-caret-down"></i></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">House & Garden</a></li>
                                    <li><a href="#">Childrens & Parents</a></li>
                                    <li><a href="#">Fashion & Beauty</a></li>
                                    <li><a href="#">Sport & Camping</a></li>
                                    <li><a href="#">Leisure & Entertainment</a></li>
                                    <li><a href="#">Vehicle</a></li>
                                    <li><a href="#">Medicine</a></li>
                                    <li><a href="#">Electronics</a></li>
                                    <li><a href="#">Other Stuff</a></li>
                                </ul>
                            </div>
                            <input type="text" class="form-control" placeholder="Search for items / people / groups" id="search">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 right-responsive  pull-right">
                <div class="right-notify-area">


                    <a href="javascript:void(0)" class="post-btn pull-left"><i class="fa fa-plus"></i> post </a>

                    <div class="account-info pull-left">
                        <a href="javascript:void(0)" class="open-profile"><img alt="..." src="assets/images/userimg.png"
                                                                               class="img-responsive">
                            <span>Anonymous</span>

                            <div class="clearfix"></div>
                        </a>

                        <ul>
                            <li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
                            <li><a href="front/home/logout.html"><i class="fa fa-sign-out"></i> Log Out</a></li>
                        </ul>
                    </div>
                    <div class="pull-right">
                        <div class="notification-box  pull-left">
                            <a href="javascript:void(0)" class="open-notifications"><i class="fa fa-globe"></i><span
                                    class="alert-message">1</span></a>

                            <ul>
                                <li><h3>Notifications</h3></li>
                                <li>
                                    <a href="javascript:void(0)" class="notification-row">
                                        <img src="assets/images/userimg.png" alt="image">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                                        <p>updates. Tasty Shoppers</p>
                                        <span>2 hour ago</span>

                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="notification-row">
                                        <img src="assets/images/userimg.png" alt="image">

                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                                        <p>updates. Tasty Shoppers</p>
                                        <span>2 hour ago</span>

                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="notification-box message-box pull-left">
                            <a href="javascript:void(0)" class="open-message"><i class="fa fa-list-alt"></i><span
                                    class="alert-circle">6</span></a>
                            <ul>
                                <li><h3>inbox</h3></li>
                                <li>
                                    <div class="notification-row">
                                        <img src="assets/images/userimg.png" alt="image">

                                        <div class="content-sec">
                                            <h4>Dean Jones(2)</h4>

                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                            <span>Aug 14</span>

                                            <div class="message-controls">
                                                <a href="javascript:void(0)" class="read-unread-control"></a>
                                                <a href="javascript:void(0)" class="cancel-btn">x</a>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                    </div>
                                </li>
                                <li>
                                    <div class="notification-row">
                                        <img src="assets/images/userimg2.png" alt="image">

                                        <div class="content-sec">
                                            <h4>Jones Dean (1)</h4>

                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                            <span>Aug 14</span>

                                            <div class="message-controls">
                                                <a href="javascript:void(0)" class="read-unread-control cread"></a>
                                                <a href="javascript:void(0)" class="cancel-btn">x</a>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                    </div>
                                </li>
                                <li>
                                    <div class="notification-row">
                                        <img src="assets/images/userimg3.png" alt="image">

                                        <div class="content-sec">
                                            <h4>Tom Hardy (1)</h4>

                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                            <span>Aug 14</span>

                                            <div class="message-controls">
                                                <a href="javascript:void(0)" class="read-unread-control cread"></a>
                                                <a href="javascript:void(0)" class="cancel-btn">x</a>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                    </div>
                                </li>
                                <li><a href="javascript:void(0)">See All</a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Top Header End Here -->


<!-- Navigation area start Here -->
<div class="navigation-area">

    <div class="col-sm-12">
        <div class="row">
            <div class="navbar-header">
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse"
                        data-target=".bs-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="glyphicon glyphicon-align-justify"></span>
                </button>
            </div>
            <nav class="collapse navbar-collapse bs-navbar-collapse padding-right-0">
                <ul class="nav navbar-nav">
                    <li><a href="javascript:void(0)">House & Garden</a></li>
                    <li><a href="javascript:void(0)">Children & Parents</a></li>
                    <li><a href="javascript:void(0)">Fashion & Beauty</a></li>
                    <li><a href="javascript:void(0)">Sport & Camping</a></li>
                    <li><a href="javascript:void(0)">Leisure & Entertainment</a></li>
                    <li><a href="javascript:void(0)">Vehicle</a></li>
                    <li><a href="javascript:void(0)">Medicine</a></li>
                    <li><a href="javascript:void(0)">Electronics</a></li>
                    <li><a href="javascript:void(0)">Other Stuff</a></li>
                    <li><a href="javascript:void(0)">Terms of Use</a></li>
                    <li><a href="javascript:void(0)">Privacy Policy</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Navigation area end Here --><!-- category area start here -->
<div class="category-area">

<div class="row">
<div class="product-container">
<div class="masonry">
<div class="products-box ">
    <div class="invite-list-box">
        <span>Invite Friends To Tasty Shoppers <i class="fa fa-arrow-right"></i></span>

        <div class="clearfix"></div>
        <div class="postby-area">
            <img alt="..." src="assets/images/userimg5.png" class="img-responsive">
            <span>Marry Jane</span>
            <a href="javascript:void(0)">invite</a>

            <div class="clearfix"></div>
        </div>
        <div class="postby-area">
            <img alt="..." src="assets/images/userimg6.png" class="img-responsive">
            <span>Thomas </span>
            <a href="javascript:void(0)">invite</a>

            <div class="clearfix"></div>
        </div>
        <div class="postby-area">
            <img alt="..." src="assets/images/userimg7.png" class="img-responsive">
            <span>Dean Jones</span>
            <a href="javascript:void(0)">invite</a>

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <a href="javascript:void(0)" class="btn btn-primary"> <i class="fa fa-plane"></i> Invite Friends</a>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img1.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup"
               class="btn quik-view-product btn-primary"> Buy it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img2.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg7.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img3.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg6.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img  src="assets/images/login-img.png" class="img-responsive" alt="img">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg5.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>

<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img5.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg4.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img6.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg3.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img7.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg2.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img8.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img14.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg7.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img12.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg6.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img8.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg5.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img12.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg4.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img8.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg3.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="products-box ">
    <div class="img-section">
        <img alt="..." src="assets/images/img14.png" class="img-responsive">

        <div class="img-overlay">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#product-popup" class="btn btn-primary"> Buy
                it</a>
            <button type="button" class="btn  btn-default">
                <i class="fa fa-share-square-o"></i> Tasty Share
            </button>
            <button type="button" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-heart"></span>
            </button>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
        </div>
    </div>
    <div class="control-section">
        <div class="controls">
            <span><i class="fa fa-heart"></i> 2</span>
            <span><i class="fa fa-comment"></i> 0</span>
            <span><i class="fa fa-shopping-cart"></i> 0</span>
        </div>
        <span class="price pull-right">159.99$</span>

        <div class="clearfix"></div>
    </div>
    <div class="postby-area">
        <img alt="..." src="assets/images/userimg2.png" class="img-responsive">
        <span>dapper dandy</span>

        <div class="clearfix"></div>
    </div>
</div>
</div>
</div>
</div>

</div>
<!-- category End here -->

<!-- Chat box section Start Here -->
<a href="javascript:void(0)" class="chat-icon open-chat"><img alt="..." src="assets/images/chaticon.png"
                                                              class="img-responsive"/><span class="num-notification">2</span></a>

<div class="chatbox-area">
    <div class="chat-header">
        <span>Tasty Chat</span>
        <a href="javascript:void(0)" class="close-chat"><i class="fa fa-times"></i></a>
    </div>
    <div class="chat-body">
        <div class="chat-users">
            <ul>
                <li>
                    <a href="javascript:void(0)">
                        <img alt="..." src="assets/images/userimg.png"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <img alt="..." src="assets/images/userimg2.png"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <img alt="..." src="assets/images/userimg3.png"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <img alt="..." src="assets/images/userimg4.png"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <img alt="..." src="assets/images/userimg5.png"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <img alt="..." src="assets/images/userimg6.png"/>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <img alt="..." src="assets/images/userimg7.png"/>
                    </a>
                </li>
                <li class="clearfix"></li>
            </ul>
            <span class="more-user-count">+30</span>
        </div>
        <div class="chat-content">
            <ul>
                <li>
                    <div class="chat-user-img">
                        <img alt="..." src="assets/images/chatboxuserimg.png">
                    </div>
                    <div class="chat-message">
                        <div class="message-wrape">
                            <span class="user-name">Dean Jones</span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                        <span class="posted-date">Aug 14</span>
                    </div>

                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="chat-user-img">
                        <img alt="..." src="assets/images/chatboxuserimg1.png">
                    </div>
                    <div class="chat-message">
                        <div class="message-wrape">
                            <span class="user-name">Dean Jones</span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
                        </div>
                        <span class="posted-date">Aug 14</span>
                    </div>
                    <div class="clearfix"></div>

                </li>
                <li>
                    <div class="chat-user-img">
                        <img alt="..." src="assets/images/chatboxuserimg2.png">
                    </div>
                    <div class="chat-message with-img">
                        <div class="message-wrape" style="background-image:url('../185.26.182.217/Rqae5eb53b-c103-49e4-a363-9cb05f81161d/ID8AFC546752F9982D/RV200000/AVSkyController_3.2.4.52865/Br200/CL2-ea/DL0/DT0/EI6079755/Ht240/IP139.190.245.138_5482/IQ50/MO15/MT0/NIkh/file.19.delaye') ">
                            <span class="user-name">Dean Jones</span>

                        </div>
                        <span class="posted-date">Aug 14</span>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="chat-user-img">
                        <img alt="..." src="assets/images/chatboxuserimg3.png">
                    </div>
                    <div class="chat-message">
                        <div class="message-wrape">
                            <span class="user-name">Dean Jones</span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor inci</p>
                        </div>
                        <span class="posted-date">Aug 14</span>
                    </div>
                    <div class="clearfix"></div>

                </li>
            </ul>
        </div>
    </div>
    <div class="chat-footer">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Type a message...">

            <div class="input-group-addon">
                <a href="javascript:void(0)" class="openChatsUpload"><i class="fa fa-camera"></i></a>
                <a href="javascript:void(0)" class="openChatsSmily"><i class="fa fa-smile-o"></i></a>
            </div>
        </div>
    </div>
</div>
<!-- Chat box section End Here -->

<!-- scroller icon Start Here -->
<a href="javascript:void(0)" class="scroll-up"><img alt="..." src="assets/images/upicon.png"
                                                    class="img-responsive"/></a>
<!-- scroller End Here -->


<!-- url popup -->
<div class="url-post-popup url-popup">
    <div class="popup-head">
        <i class="glyphicon glyphicon-link"></i>
        Post URL

        <a href="javascript:void(0)" data-link="url-popup" class="closepopup">x</a>
    </div>
    <div class="popup-body">
        <label>Post URL</label>
        <input type="text" placeholder="https://www.tastyshoppers.com/f7XB2S1lIG0"/>
        <a href="javascript:void(0)" class="popup-btn-blue open-post-popup pull-left"><i class="fa fa-plus-circle"></i>
            Post to Tasty
            Shoppers</a>

        <div class="clearfix"></div>
    </div>

</div>
<!--End url popup -->

<!-- Post popup -->
<div class="modal fade" id="postPin-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="post-popup ">
            <div class="popup-head">
                <div class="icon-head">
                    <i class="fa fa-plus"></i> post
                </div>
                <a href="javascript:void(0)" data-link="post-popup" class="closepopup">x</a>
            </div>
            <div class="popup-body ">
                <div class="">
                    <div class="col-sm-3 thumbnail">
                        <img alt="image" src="assets/images/img5.png" style="height: 78px;">
                    </div>
                    <div class="col-sm-3 thumbnail">
                        <img alt="image" src="assets/images/img3.png" style="height: 78px;">
                    </div>
                    <div class="col-sm-3 thumbnail">
                        <img alt="image" src="assets/images/img6.png" style="height: 78px;">
                    </div>
                    <div class="col-sm-3 thumbnail">
                        <img alt="image" src="assets/images/img7.png" style="height: 78px;">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 inputs-area">
                        <textarea placeholder="add a product description"></textarea>

                        <div class="upload-btn-area">
                            <a href="javascript:void(0)" class="btn-file-input text-center popup-btn-blue"><i
                                    class="fa m-right-10 fa-camera"></i>Upload Image</a>
                            <input type="file" class="file-input">
                        </div>
                        <input placeholder="0" type="text">
                        <select>
                            <option>Post to a group</option>
                        </select>

                        <div class="post-success"><i class="fa fa-check-circle"></i> post to social wall</div>
                        <div class="btn-controls">
                            <a href="javascript:void(0)" class="popup-btn-blue width65per pull-left"><i
                                    class="fa m-right-10 fa-plus-circle"></i>Post to Tasty Shoppers</a>
                            <a href="javascript:void(0)" class="popup-gray-btn width30per pull-right">Cancel</a>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="product-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-sm-6 left-slider-box">
                    <div class="share-box pull-right">
                        <div class="share-box-head">
                            <i class="fa fa-share-square-o"></i>
                            share
                        </div>
                        <div class="share-body">
                            <a href="#">
                                <div class=" share-area">
                                    <img alt="image" src="assets/images/fb-icon.png">
                                    <span class="like-count">8</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class=" share-area">
                                    <img alt="image" src="assets/images/twticon.png">
                                    <span class="like-count">10</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class=" share-area">
                                    <img alt="image" src="assets/images/gmailicon.png">
                                    <span class="like-count">10</span>
                                </div>
                            </a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <ul class="quick-product-slide">
                            <li><img alt="image" src="../185.26.182.217/Rqae5eb53b-c103-49e4-a363-9cb05f81161d/ID8AFC546752F9982D/RV200000/AVSkyController_3.2.4.52865/Br200/CL2-ea/DL0/DT0/EI6092283/Ht240/IP139.190.245.138_5487/IQ50/MO15/MT0/NIkh/file.1b.delaye"></li>
                            <li><img alt="image" src="../185.26.182.217/Rqae5eb53b-c103-49e4-a363-9cb05f81161d/ID8AFC546752F9982D/RV200000/AVSkyController_3.2.4.52865/Br200/CL2-ea/DL0/DT0/EI6092283/Ht240/IP139.190.245.138_5487/IQ50/MO15/MT0/NIkh/file.1b.delaye"></li>
                            <li><img alt="image" src="../185.26.182.217/Rqae5eb53b-c103-49e4-a363-9cb05f81161d/ID8AFC546752F9982D/RV200000/AVSkyController_3.2.4.52865/Br200/CL2-ea/DL0/DT0/EI6092283/Ht240/IP139.190.245.138_5487/IQ50/MO15/MT0/NIkh/file.1b.delaye"></li>
                            <li><img alt="image" src="../185.26.182.217/Rqae5eb53b-c103-49e4-a363-9cb05f81161d/ID8AFC546752F9982D/RV200000/AVSkyController_3.2.4.52865/Br200/CL2-ea/DL0/DT0/EI6092283/Ht240/IP139.190.245.138_5487/IQ50/MO15/MT0/NIkh/file.1b.delaye"></li>
                            <li><img  alt="image" src="../185.26.182.217/Rqae5eb53b-c103-49e4-a363-9cb05f81161d/ID8AFC546752F9982D/RV200000/AVSkyController_3.2.4.52865/Br200/CL2-ea/DL0/DT0/EI6092283/Ht240/IP139.190.245.138_5487/IQ50/MO15/MT0/NIkh/file.1b.delaye"></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <ul id="quick-product-thumb-slide" class="owl-carousel">
                            <li><a href="javascript:void(0)" class=""><img alt="image" src="assets/images/PRODUCT-THUMN.png"></a>
                            </li>
                            <li><a href="javascript:void(0)" class=""><img alt="image" src="assets/images/PRODUCT-THUMN.png"></a>
                            </li>
                            <li><a href="javascript:void(0)" class=""><img alt="image" src="assets/images/PRODUCT-THUMN.png"></a>
                            </li>
                            <li><a href="javascript:void(0)" class=""><img alt="image" src="assets/images/PRODUCT-THUMN.png"></a>
                            </li>
                            <li><a href="javascript:void(0)" class=""><img alt="image" src="assets/images/PRODUCT-THUMN.png"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 right-area">
                    <h2>MID 2 BLUEBERRY</h2>
                    <span class="buket"><i class="fa fa-shopping-cart"></i> Purchase:</span>
                    <span class="price">49.99$ at Etsy <img alt="image" src="assets/images/eimg.png"/> </span>
                    <a href="#" class="btn btn-primary col-sm-9 blue-btn"> Buy Now</a>

                    <div class="clearfix"></div>
                    <div class="client-info-area">
                        <img alt="image" src="assets/images/userimg.png" class="pull-left">

                        <div class="info-area pull-left">
                            <span class="user-name">Amia kurs</span>
                            <a href="#" class="post-btn">follow</a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="comment-area-list">
                        <div class="comment-area">
                            <img alt="image" src="assets/images/userimg.png" class="pull-left">

                            <div class="info-area pull-left">
                                <span class="user-name">Amia kurs</span>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt
                                    ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="comment-area">
                            <img alt="image" src="assets/images/userimg2.png" class="pull-left">

                            <div class="info-area pull-left">
                                <span class="user-name">Amia kurs</span>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt
                                    ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="comment-area">
                            <img alt="image" src="assets/images/userimg3.png" class="pull-left">

                            <div class="info-area pull-left">
                                <span class="user-name">Amia kurs</span>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt
                                    ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <a href="#" class="view-comment">View more comments</a>

                    <div class="comment-post-area">
                        <img alt="image" src="assets/images/userimg6.png">

                        <div class="input-area">
                            <input type="text" placeholder="write a comment ..." class="comment-area pull-left">

                            <div class="pull-right">
                                <a href="#"><i class="fa fa-camera"></i></a>
                                <a href="#"><i class="fa fa-smile-o"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- chat uploads popup -->
<div class="modal fade" id="chatUploadPopup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="post-popup ">
            <div class="popup-head">
                <div class="icon-head">
                    upload files
                </div>
            </div>
            <div class="popup-body ">
                <div class="">
                    <div class="col-sm-3 thumbnail">
                        <img alt="image" src="assets/images/img5.png" style="height: 78px;">
                    </div>
                    <div class="col-sm-3 thumbnail">
                        <img alt="image" src="assets/images/img3.png" style="height: 78px;">
                    </div>
                    <div class="col-sm-3 thumbnail">
                        <img alt="image" src="assets/images/img6.png" style="height: 78px;">
                    </div>
                    <div class="col-sm-3 thumbnail">
                        <img alt="image" src="assets/images/img7.png" style="height: 78px;">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 inputs-area">
                        <!--<div class="dragUploadArea">Drag Your Images Here</div>-->
                        <div class="upload-btn-area">
                            <a href="javascript:void(0)" class="btn-file-input text-center popup-btn-blue"><i
                                    class="fa m-right-10 fa-camera"></i>Upload Image</a>
                            <input type="file" class="file-input">
                        </div>

                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- chat uploads popup -->
<div class="modal fade" id="chatSmilyPopup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="post-popup ">
            <div class="popup-head">
                <div class="icon-head">
                    select emotions
                </div>
            </div>
            <div class="popup-body ">
                <ul class="list-inline">
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/1.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/2.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/3.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/4.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/5.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/6.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/7.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/8.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/9.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/10.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/11.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/12.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/13.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/14.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/15.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/16.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/17.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/18.png" width="30px" /> </a></li>
                    <li><a href="javascript:void(0)"><img src="assets/images/smilies/19.png" width="30px" /> </a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>    