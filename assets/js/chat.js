/**
 * Created by TheDeveloper on 9/5/2015.
 */
$.prototype.privateChat = function(task, data){
        var obj = this;
        var makeHMTL = function(data){
            console.log('-----makeHTML--------');
            console.log(data);
            //if(data.msgs.length){
            var fianl_template = "";
            console.log(data.msgs);
                $.each(data.msgs, function(i, v){
                    console.log(v);
                    if(v.type=='sender'){
                        var template = $('#sender_msg_template').html();
                        template = template.replace("{{img_thumbnail}}", v.img);
                        template = template.replace("{{sender_name}}", v.name);
                        template = template.replace("{{sender_date}}", v.time);
                        template = template.replace("{{sender_message}}", v.msg);
                        console.log(template)
                    }else if(v.type=='receiver'){
                        var template = $('#reciever_msg_template').html();
                        template = template.replace("{{img_thumbnail}}", v.img);
                        template = template.replace("{{reciever_name}}", v.name);
                        template = template.replace("{{reciever_date}}", v.time);
                        template = template.replace("{{reciever_message}}", v.msg);
                        console.log(template)

                    }
                    fianl_template = fianl_template+template;
                });
            //}
            return fianl_template;
        };
        var appendContent = function(content, data){
            $(data.appendTo).append(content);
        };
        var putContent = function(content, data){
            $(data.appendTo).html(content);
        };
        var makeConversationListingHTML = function(data){
            var fianl_template = "";
            $.each(data.mesg_listing, function(i, v){
                if(typeof v !='undefined'){
                    console.log(v);
                    var template = $('#conversations_listing_item_template').html();
                    template = template.replace(/{{user_id}}/g, v.uid);
                    template = template.replace('{{img_thumbnail}}', v.img);
                    template = template.replace(/{{user_name}}/g, v.username);
                    template = template.replace('{{last_message}}', v.last_msg);
                    template = template.replace('{{message_time}}', v.msg_time);
                    if(v.unread_count == '0' || v.unread_count == 0){
                        template = template.replace('{{hdn_classs}}', 'hidden');
                    }else{
                        template = template.replace('{{hdn_classs}}', '');
                        template = template.replace('{{unread_count}}', v.unread_count);
                    }

                        fianl_template = fianl_template+template;
                }


            });
            return fianl_template;
        };
        var clearMsgBox = function(){
          $('#sender_new_message').val('');
        };
        var fillConversationListing = function(content, data){
            $(data.appendTo).html(content);
        };



        var getConversationListing = function(data){
            $.ajax({
                type : data.type,
                url : data.url,
                data : {csrf_test_name : $('input[name=csrf_test_name]').val()},
                success : function(response){
                    var response_parsed = $.parseJSON(response);
                    init('private-conversation-listing', response_parsed);
                    jQuery('#conversation_listing_data').val(response);

                }
            });
        };

    var changeUrl = function (page, url)
        {
            if (typeof (history.pushState) != "undefined") {
                var obj = {Page : page, Url : url};
                history.pushState(obj, obj.Page, obj.Url);
            } else {
                alert("Browser does not support HTML5.");
            }
        }

    var getUserConversation = function(data){
        $.ajax({
            type : 'POST',
            url : hostName+"/messages/get-user-conversation",
            data : {csrf_test_name : $('input[name=csrf_test_name]').val(), uid:data.uid, updateReceiverStatus : data.updateReceiverStatus},
            success : function(response){
                console.log('afetr');
                console.log(response);
                var response_parsed = $.parseJSON(response);
                console.log(response_parsed);
                console.log('----------------');
                console.log(response_parsed);

                console.log('----------------');
                var content = makeHMTL(response_parsed);
                putContent(content, response_parsed);
                scrollDownMessageArea();
                /*var response_parsed = $.parseJSON(response);
                init('private-conversation-listing', response_parsed);
                jQuery('#conversation_listing_data').val(response);*/

            }
        });
    };

    var updateRecieverId = function(id){
        jQuery('#reciever_id').val(id);

    };

    var refreshChat = function(){
        if(jQuery('#reciever_id').val() != ''){
            var data = {
                uid : jQuery('#reciever_id').val(),
                updateReceiverStatus : 0
            };
            getUserConversation(data);
        }
    };
    var scrollDownMessageArea = function(){
        var wtf    = $('.message-conversation-box');
        var height = wtf[0].scrollHeight;
        wtf.scrollTop(height);
    };

    var recurrenceFunctions = function(settings){

        setInterval(function(){
            refreshChat();

        }, settings.interval)

    }


        var init = function(task, data){
            if(task=='privateChatBox'){
                $.ajax({
                    type : 'POST',
                    url : hostName+"/messages/send-message",
                    data : {csrf_test_name : $('input[name=csrf_test_name]').val(), message:data.message, rid : data.receiver_id},
                    success : function(response){
                        console.log(response);
                        var response_parsed = $.parseJSON(response);
                        if(response_parsed.status == '1'){
                            var data_for_convrsation = {
                                uid : data.receiver_id,
                                updateReceiverStatus : 0
                            };
                            getUserConversation(data_for_convrsation);
                            scrollDownMessageArea();
                            clearMsgBox();


                        }
                        /*console.log(response_parsed);
                        console.log('----------------');
                        console.log(response_parsed);

                        console.log('----------------');
                        var content = makeHMTL(response_parsed);
                        putContent(content, response_parsed);*/
                        /*var response_parsed = $.parseJSON(response);
                         init('private-conversation-listing', response_parsed);
                         jQuery('#conversation_listing_data').val(response);*/
                    }
                });

                /*var content = makeHMTL(data);
                appendContent(content, data);*/
                //clearMsgBox();
            }else if(task=='private-conversation-listing'){
                var content = makeConversationListingHTML(data);
                fillConversationListing(content, data);
            }else if(task == 'getConversationListing'){
                getConversationListing(data);

                if(typeof data.repetition != 'undefined' && data.repetition){
                    var time_interval = 20000;
                    if(typeof data.time_interval != 'undefined' ){
                        time_interval = data.time_interval;
                    }
                    setInterval(function(){ getConversationListing(data) }, time_interval);
                }
            }
            else if(task == 'getUserConversation'){
                changeUrl(data.attr('data-uname') + ' Messages', '/messages/'+data.attr('data-uid'))
                var data_for_convrsation = {
                    uid : data.attr('data-uid'),
                    updateReceiverStatus : 1
                };
                getUserConversation(data_for_convrsation);
                updateRecieverId(data.attr('data-uid'));
            }
            else if(task == 'recurrenceFunctions'){
                recurrenceFunctions(data);
            }
        };

    init(task, data);

};
window.onload = $.prototype.privateChat('recurrenceFunctions', {interval : 3000});










