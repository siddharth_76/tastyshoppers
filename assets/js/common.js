(function () {


    var adjustMessageBoxHeight = function(){
        var windowHeight = $(window).height(),
            scrollerMaxHeight = windowHeight-$(".top-header").height()-$(".navigation-area").height()-165;
            $(".message-list-box .conversations-listing").css("height",scrollerMaxHeight+"px");
        if ($(window).width() > 990) {
            $(".message-conversation-box-main").css("height",$(".message-list-box").height()+"px");
            setTimeout(function () {

                var msgtextHeight = $(".message-text-box").height();
                $(".message-text-box .input-area").css("min-height", msgtextHeight / 2);
                $(".message-text-box .input-area input").css("line-height", msgtextHeight / 2 + "px");
                $(".message-text-box .controls-area").height(msgtextHeight / 2);
            }, 100);
        }else{
            $(".message-list-box .conversations-listing").css("height",scrollerMaxHeight+100+"px");
            var msgtextHeight = $(".message-text-box").height();
            $(".message-conversation-box-main").css("height",$(".message-list-box").height()+"px");
            $(".message-text-box .input-area").css("min-height", msgtextHeight / 2);
            $(".message-text-box .input-area input").css("line-height", msgtextHeight / 2 + "px");
        }


    };
    var adjustMessagePagePop = function(){
        if ($(window).width() <= 990) {
            var fixedDiv = $(".message-list-box").parent(".col-md-4"),
                listingDiv = $(".message-list-box .conversations-listing"),
                listingDivHead = $(".message-list-box .message-search-head");
            fixedDiv.css({
                "position": "fixed",
                "height": $(window).height() + "px",
                "top": "0px",
                "left": "-315px",
                "padding": "0px",
                "width": "315px",
                "z-index": "999999"
            });
            listingDiv.css({
                "height": ($(window).height() - listingDivHead.height()) + "px"
            });
            $(".listing-trigger").animate({"right":"-50px"});
        }else{
            $(".message-list-box").parent(".col-md-4").removeAttr("style");
            var windowHeight = $(window).height(),
                scrollerMaxHeight = windowHeight - $(".top-header").height() - $(".navigation-area").height() - 165;
            $(".message-list-box .conversations-listing").css("height", scrollerMaxHeight + "px");
        }
    };
    $(function () {


        //hide loginpopup
        $("body").on("click",".continue-hide-popup",function(){
            $("#login-popup").modal("hide")
        });
        var clickM = 0;
        $("body").on("click",".listing-trigger",function(){
            if(clickM === 0){
                $(".message-list-box").parent(".col-md-4").animate({"left":"0px"});
                $(this).animate({"right":"0px"});
                clickM = 1;
            }else{
                clickM =0;
                $(".message-list-box").parent(".col-md-4").animate({"left":"-315px"});
                $(this).animate({"right":"-50px"});
            }

        });


        $(window).scroll(function () {

            var scollPosition = $(window).scrollTop();

            if (scollPosition > 0) {

                $(".scroll-up").fadeIn();

            } else if (scollPosition < 1) {

                $(".scroll-up").fadeOut();

            }
        });


        $(".scroll-up").click(function () {
            $("body").animate({scrollTop: 0}, 800);
           // $("window").scrollTop(0);

           // $('body').animate({scrollTop: $('body').get(0).scrollHeight}, 3000);
        });

        $("body").on("click", ".close-chat", function () {

            $(".chatbox-area").hide();

        });

        $("body").on("click", ".open-chat", function (e) {
            var chatEle = $(".chatbox-area");

            if (chatEle.is(":visible")) {

                chatEle.hide();

            } else {

                chatEle.show();

                // for put scroll on chat box div
                $(".chatbox-area .chat-body .chat-content").niceScroll({cursorcolor:"#7a808f",cursoropacitymax:0.7,touchbehavior:true});
            }
        });

        $("body").on("click", ".open-notifications", function () {
            $(".open-message").next("ul").hide();
            $(".open-profile").next("ul").hide();
            if ($(this).next("ul").is(":visible")) {
                $(this).next("ul").hide();
            } else {
                $(this).next("ul").show();
            }

        });

        $("body").on("click", ".open-message", function () {
            $(".open-notifications").next("ul").hide();
            $(".open-profile").next("ul").hide();
            if ($(this).next("ul").is(":visible")) {
                $(this).next("ul").hide();
            } else {
                $(this).next("ul").show();
            }

        });

        $("body").on("click", ".open-profile", function () {
            $(".open-notifications").next("ul").hide();
            $(".open-message").next("ul").hide();
            if ($(this).next("ul").is(":visible")) {
                $(this).next("ul").hide();
            } else {
                $(this).next("ul").show();
            }

        });

        $("body").on("click", ".post-btn", function () {

            if ($(".url-popup").is(":visible")) {
                $(".url-popup").hide();
            } else {
                $(".url-popup").show();
            }

        });

        $("body").on("click", ".open-post-popup", function () {
            $(".url-popup").hide();
            $("#postPin-popup").modal("show");

        });

        $("body").on("click", ".share-product", function () {

            if ($(".url-popup").is(":visible")) {
                $(".url-popup").hide();
            } else {
                $(".url-popup").show();
            }

        });

        $("body").on("click", ".btn-file-input", function () {

            $(this).next(".file-input").trigger("click");

        });

        $("body").on("click", ".openChatsUpload", function () {

            $("#chatUploadPopup").modal("show");
            $("#chatSmilyPopup").modal("hide");
        });

        $("body").on("click", ".openChatsSmily", function () {
            $("#chatUploadPopup").modal("hide");
            $("#chatSmilyPopup").modal("show");

        });

        $("body").on("click", ".closepopup", function () {

            var linkedEle = $(this).attr("data-link");
            if(linkedEle == "post-popup"){
                $("#postPin-popup").modal("hide");
            }else{
                $("." + linkedEle).hide();
            }


        });
        adjustMessageBoxHeight();
        adjustMessagePagePop();
        $(window).load(function () {


        });







        //call functions on resize
        $(window).resize(function(){
            adjustMessageBoxHeight();
            adjustMessagePagePop();
        });


    });

}(jQuery));


(function ($) {
    "use strict";
    function centerModal() {
        $(this).css('display', 'block');
        var $dialog  = $(this).find(".modal-dialog"),
        offset       = ($(window).height() - $dialog.height()) / 2,
        bottomMargin = parseInt($dialog.css('marginBottom'), 10);

        // Make sure you don't hide the top part of the modal w/ a negative margin if it's longer than the screen height, and keep the margin equal to the bottom margin of the modal
        if(offset < bottomMargin) offset = bottomMargin;
        $dialog.css("margin-top", offset);
    }

    $(document).on('show.bs.modal', '.modal', centerModal);
    $(window).on("resize", function () {
        $('.modal:visible').each(centerModal);
    });
}(jQuery));



