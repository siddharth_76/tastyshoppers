-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2015 at 02:03 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tasty`
--

-- --------------------------------------------------------

--
-- Table structure for table `ts_attachments`
--

CREATE TABLE IF NOT EXISTS `ts_attachments` (
`attachment_id` int(11) NOT NULL,
  `attachment_type` enum('profile') DEFAULT NULL,
  `attachment_url` text,
  `user_id` int(11) DEFAULT NULL,
  `data_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ts_attachments`
--

INSERT INTO `ts_attachments` (`attachment_id`, `attachment_type`, `attachment_url`, `user_id`, `data_id`) VALUES
(1, 'profile', 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xat1/v/t1.0-1/p50x50/11165057_10206488014189446_2820853966100219487_n.jpg?oh=1e1fc1df5da3907c6ed0b5654d3e9c2c&oe=565CEC12&__gda__=1453923637_8f34592fbf1e042b6b1b0cc9f6118274', 1, 1),
(3, 'profile', 'http://pbs.twimg.com/profile_images/378800000368669548/1456c742c18d6351cafb7104c4601b04_normal.jpeg', 3, 3),
(4, 'profile', 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpf1/v/t1.0-1/p50x50/11933442_999778273419102_2051560922205899038_n.jpg?oh=ea5eea2359b1f7dadff26164d3b9db25&oe=56A780BF&__gda__=1453214484_f909df7203e91a69db4c079722aea3b7', 4, 4),
(5, 'profile', 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfa1/v/t1.0-1/c37.37.466.466/s50x50/1044288_134158123457727_133442038_n.jpg?oh=f4599b3c9dd4073f2211da3e4519d6b8&oe=569DD86E&__gda__=1453717086_c37aa0200a72876409e46736c9a6a7f7', 5, 5),
(6, 'profile', 'https://lh3.googleusercontent.com/-1xa2X6zEkMk/AAAAAAAAAAI/AAAAAAAAACQ/PqQKTT90OkE/photo.jpg', 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `ts_chat`
--

CREATE TABLE IF NOT EXISTS `ts_chat` (
`id` int(100) NOT NULL,
  `to` int(10) NOT NULL,
  `from` int(10) NOT NULL,
  `message` text NOT NULL,
  `time` varchar(200) NOT NULL,
  `sender_read` int(200) NOT NULL DEFAULT '1',
  `receiver_read` int(200) NOT NULL DEFAULT '0',
  `sender_deleted` int(200) NOT NULL DEFAULT '0',
  `receiver_deleted` int(200) NOT NULL DEFAULT '0',
  `file` text
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ts_chat`
--

INSERT INTO `ts_chat` (`id`, `to`, `from`, `message`, `time`, `sender_read`, `receiver_read`, `sender_deleted`, `receiver_deleted`, `file`) VALUES
(1, 3, 4, 'Hello...', '1441740971', 1, 1, 0, 0, ' '),
(2, 4, 3, 'Hy...', '1441740999', 1, 1, 0, 0, NULL),
(3, 1, 3, 'Hello Kamran, Ali Here', '1441818740', 1, 1, 0, 0, NULL),
(4, 1, 3, 'This is my second messge to you kamran. Ali Here', '1441818840', 1, 0, 0, 0, NULL),
(5, 1, 3, 'This is 3rd Message to Kamran from Ali', '1441830370', 1, 0, 0, 0, NULL),
(6, 1, 3, 'this is latest message to kamran', '1441830630', 1, 0, 0, 0, NULL),
(7, 3, 4, 'this is new from agha to kamran', '1441833344', 1, 1, 0, 0, NULL),
(8, 4, 3, 'this is from Kamran to agha', '1441837633', 1, 1, 0, 0, NULL),
(9, 4, 3, 'New msg from tariq to kamran', '1442012306', 1, 1, 0, 0, NULL),
(10, 4, 5, 'falak here', '1442013050', 1, 1, 0, 0, NULL),
(11, 4, 5, 'Heloooooooooooooooooooooo', '1442060298', 1, 1, 0, 0, NULL),
(12, 5, 4, 'from kamran to falak', '1442261539', 1, 1, 0, 0, NULL),
(13, 4, 3, 'hello , i am tariq, just want to say LOLLLLZZ', '1442268288', 1, 1, 0, 0, NULL),
(14, 4, 3, 'hello do dear baby ', '1442268355', 1, 1, 0, 0, NULL),
(15, 4, 5, 'this is from falak to kamran hahahaha', '1442269081', 1, 1, 0, 0, NULL),
(16, 5, 4, 'Hello', '1442336904', 1, 1, 0, 0, NULL),
(17, 5, 4, 'hehehehe', '1442336966', 1, 1, 0, 0, NULL),
(18, 5, 4, 'hello falak :)', '1442337284', 1, 1, 0, 0, NULL),
(19, 5, 4, 'hello falak', '1442337305', 1, 1, 0, 0, NULL),
(20, 4, 5, 'Hello Kamran', '1442337514', 1, 1, 0, 0, NULL),
(21, 5, 4, 'how are you?', '1442337568', 1, 1, 0, 0, NULL),
(22, 4, 5, 'i am fine', '1442338422', 1, 1, 0, 0, NULL),
(23, 4, 5, 'what about you?', '1442338478', 1, 1, 0, 0, NULL),
(24, 5, 4, 'yes i am fine', '1442338838', 1, 1, 0, 0, NULL),
(25, 4, 5, 'good', '1442338929', 1, 1, 0, 0, NULL),
(26, 5, 4, 'how old are you?', '1442339089', 1, 1, 0, 0, NULL),
(27, 4, 5, 'i am of 22, nd you?', '1442339213', 1, 1, 0, 0, NULL),
(28, 4, 5, 'are you there jaaanu?', '1442339231', 1, 1, 0, 0, NULL),
(29, 4, 5, 'ummmmah :*', '1442339244', 1, 1, 0, 0, NULL),
(30, 5, 4, 'i am of 9', '1442339289', 1, 1, 0, 0, NULL),
(31, 5, 4, 'g baaaji, main idar hi houn :P', '1442339306', 1, 1, 0, 0, NULL),
(32, 5, 4, 'kahan gae baaji?', '1442339380', 1, 1, 0, 0, NULL),
(33, 4, 5, 'beta main  idahr hi houn....', '1442339410', 1, 1, 0, 0, NULL),
(34, 5, 4, 'acha baji', '1442340661', 1, 1, 0, 0, NULL),
(35, 4, 5, 'hmmmm.. or sunao beta', '1442340681', 1, 1, 0, 0, NULL),
(36, 4, 5, 'kya krtay ho?', '1442341538', 1, 1, 0, 0, NULL),
(37, 5, 4, 'kuch nai baaji bus khaana khaata houn or doodh peeta houn', '1442341567', 1, 1, 0, 0, NULL),
(38, 4, 5, 'hmmm good', '1442341583', 1, 1, 0, 0, NULL),
(39, 5, 4, 'dsdsd', '1442429747', 1, 1, 0, 0, NULL),
(40, 5, 4, 'sdsdsd', '1442429756', 1, 1, 0, 0, NULL),
(41, 5, 4, 'sasasa', '1442429793', 1, 1, 0, 0, NULL),
(42, 5, 4, 'hehehehe', '1442429822', 1, 1, 0, 0, NULL),
(43, 5, 4, 'DSDSD', '1442429919', 1, 1, 0, 0, NULL),
(44, 5, 4, 'DSDS', '1442429941', 1, 1, 0, 0, NULL),
(45, 5, 4, 'ghghgh', '1442429964', 1, 1, 0, 0, NULL),
(46, 5, 4, 'lala lala', '1442430069', 1, 1, 0, 0, NULL),
(47, 5, 4, 'jaaanu', '1442430277', 1, 0, 0, 0, NULL),
(48, 3, 4, 'Hello Tariq', '1442430304', 1, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ts_sessions`
--

CREATE TABLE IF NOT EXISTS `ts_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ts_sessions`
--

INSERT INTO `ts_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('35be492f3390318462ad93771715d865', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36', 1442491321, ''),
('878fb1bc8dec2393ca4e05a7dcc09180', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36', 1442491297, ''),
('9cfb6b7154a060131d43c915fb5f4cab', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36', 1442430456, 'a:6:{s:9:"user_data";s:0:"";s:7:"user_id";s:1:"4";s:8:"fullname";s:12:"Kamran Latif";s:7:"picture";s:220:"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpf1/v/t1.0-1/p50x50/11933442_999778273419102_2051560922205899038_n.jpg?oh=ea5eea2359b1f7dadff26164d3b9db25&oe=56A780BF&__gda__=1453214484_f909df7203e91a69db4c079722aea3b7";s:12:"is_logged_in";b:1;s:14:"logged_in_type";s:8:"Facebook";}'),
('cd463b23449adfd303e9d5427b38fc6d', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36', 1442430153, 'a:6:{s:9:"user_data";s:0:"";s:7:"user_id";s:1:"4";s:8:"fullname";s:12:"Kamran Latif";s:7:"picture";s:220:"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpf1/v/t1.0-1/p50x50/11933442_999778273419102_2051560922205899038_n.jpg?oh=ea5eea2359b1f7dadff26164d3b9db25&oe=56A780BF&__gda__=1453214484_f909df7203e91a69db4c079722aea3b7";s:12:"is_logged_in";b:1;s:14:"logged_in_type";s:8:"Facebook";}'),
('d1f937fa3ec13f1c727f1c0cbaeb367b', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36', 1442432425, 'a:5:{s:7:"user_id";s:1:"4";s:8:"fullname";s:12:"Kamran Latif";s:7:"picture";s:220:"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpf1/v/t1.0-1/p50x50/11933442_999778273419102_2051560922205899038_n.jpg?oh=ea5eea2359b1f7dadff26164d3b9db25&oe=56A780BF&__gda__=1453214484_f909df7203e91a69db4c079722aea3b7";s:12:"is_logged_in";b:1;s:14:"logged_in_type";s:8:"Facebook";}');

-- --------------------------------------------------------

--
-- Table structure for table `ts_users`
--

CREATE TABLE IF NOT EXISTS `ts_users` (
`user_id` int(11) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `signup_type` enum('Facebook','Twitter','Custom','Google') DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `extras` text,
  `status_sl` tinyint(1) DEFAULT '1',
  `ts_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ts_users`
--

INSERT INTO `ts_users` (`user_id`, `fullname`, `email`, `password`, `signup_type`, `social_id`, `extras`, `status_sl`, `ts_datetime`) VALUES
(1, 'Tariq Ali', 'tariq_ali786@hotmail.com', NULL, 'Facebook', '10207293728291795', NULL, 1, '2015-09-09 07:01:04'),
(3, 'Tariq Ali', 'tariq_ali786@hotmail.com', NULL, 'Twitter', '1704147182', NULL, 1, '2015-09-11 05:22:59'),
(4, 'Kamran Latif', 'kamran_latif@ymail.com', NULL, 'Facebook', '1003178953079034', NULL, 1, '2015-09-11 16:02:22'),
(5, 'Falak Khan', 'khan.falak34@yahoo.com', NULL, 'Facebook', '471488166391386', NULL, 1, '2015-09-11 18:09:30'),
(6, 'Tariq Ali', 'tariq.webstar@gmail.com', NULL, 'Google', '106498077506335323908', NULL, 1, '2015-09-17 12:01:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ts_attachments`
--
ALTER TABLE `ts_attachments`
 ADD PRIMARY KEY (`attachment_id`);

--
-- Indexes for table `ts_chat`
--
ALTER TABLE `ts_chat`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ts_sessions`
--
ALTER TABLE `ts_sessions`
 ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `ts_users`
--
ALTER TABLE `ts_users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ts_attachments`
--
ALTER TABLE `ts_attachments`
MODIFY `attachment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ts_chat`
--
ALTER TABLE `ts_chat`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `ts_users`
--
ALTER TABLE `ts_users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
